//
//  HCAppDelegate.h
//  HomeCareSDK
//
//  Created by cyssan1991 on 06/07/2023.
//  Copyright (c) 2023 cyssan1991. All rights reserved.
//

@import UIKit;

@interface HCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
