//
//  main.m
//  HomeCareSDK
//
//  Created by cyssan1991 on 06/08/2023.
//  Copyright (c) 2023 cyssan1991. All rights reserved.
//

@import UIKit;
#import "HCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HCAppDelegate class]));
    }
}
