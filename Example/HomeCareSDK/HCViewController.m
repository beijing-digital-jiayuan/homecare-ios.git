//
//  HCViewController.m
//  HomeCareSDK
//
//  Created by cyssan1991 on 06/07/2023.
//  Copyright (c) 2023 cyssan1991. All rights reserved.
//

#import "HCViewController.h"
#import <HomeCareSDK/HomeCareSDK.h>

@interface HCViewController ()

@property (nonatomic, strong) NSArray *actionArray;

@end

@implementation HCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [HomeCareSDKManager initWithAppId:@"kyqr68n8n4r" mobile:@"13621106122" isDevelop:YES];
    
    self.actionArray = @[@"呼叫视频医生", @"获取病例列表", @"获取服务项目", @"服务项目列表", @"地址列表", @"添加地址", @"删除地址", @"更新地址", @"订单列表", @"新增订单", @"取消订单", @"订单详情", @"订单投诉", @"订单评分"];

    for (int i = 0; i < self.actionArray.count; i++) {
        UIButton *actionButton = [self actionButtonWithTitle:self.actionArray[i]];
        actionButton.frame = CGRectMake(10 + (i % 3) * 140, 44 + (i / 3) * 88, 130, 66);
        actionButton.tag = i;
        [self.view addSubview:actionButton];
    }
}

- (void)clickButton:(UIButton *)button
{
    switch (button.tag) {
        case 0:
            [HomeCareDoctorManager callDoctorWithBlock:^(HCCallingState state) {
                NSLog(@"call state : %lu", (unsigned long)state);
            }];
            break;
        case 1:
            [HomeCareDoctorManager getRecordListWithSuccess:^(NSArray<HomeCareRecordInfo *> * _Nonnull list) {

            } error:^(NSError * _Nonnull error) {
                
            }];
            break;
        case 2:
            [HomeCareServiceManager getCategoryListWithSuccess:^(NSArray<HomeCareServiceInfo *> * _Nonnull list) {

            } error:^(NSError * _Nonnull error) {

            }];
            break;
        case 3:
            [HomeCareServiceManager getServiceListWithId:@"15" success:^(NSArray<HomeCareServiceListInfo *> * _Nonnull bannerList, NSArray<HomeCareServiceListInfo *> * _Nonnull serviceList) {
                
            } error:^(NSError * _Nonnull error) {
                
            }];
            break;
        case 4:
            [HomeCareServiceManager getAddressListWithSuccess:^(NSArray<HomeCareAddressInfo *> * _Nonnull list) {
                
            } error:^(NSError * _Nonnull error) {
                
            }];
            break;
        case 5:
            [HomeCareServiceManager addAddressWithName:@"李" mobile:@"188" address:@"北京" addressLab:@"备注" block:^(BOOL success, NSError * _Nullable error) {
                
            }];
            break;
        case 6:
            [HomeCareServiceManager deleteAddressWithId:@"66" block:^(BOOL success, NSError * _Nullable error) {
                
            }];
            break;
        case 7:
            [HomeCareServiceManager editAddressWithId:@"66" name:@"刘" mobile:@"166" address:@"朝阳" addressLab:@"备注" block:^(BOOL success, NSError * _Nullable error) {
                
            }];
            break;
        case 8:
            [HomeCareServiceManager getOrderListWithSuccess:^(NSArray<HomeCareOrderInfo *> * _Nonnull list) {
                
            } error:^(NSError * _Nonnull error) {
                
            }];
            break;
        case 9:
            [HomeCareServiceManager submitOrderWithId:@"339" receiveName:@"刘" receiveMobile:@"166" receiveAddress:@"朝阳" orderDesc:@"备注" block:^(BOOL success, NSError * _Nullable error) {
                
            }];
            break;
        case 10:
            [HomeCareServiceManager cancelOrderWithId:@"131" block:^(BOOL success, NSError * _Nullable error) {
                
            }];
            break;
        case 11:
            [HomeCareServiceManager getOrderDetailWithId:@"131" success:^(HomeCareOrderInfo * _Nonnull info) {
                
            } error:^(NSError * _Nonnull error) {
                
            }];
            break;
        case 12:
            [HomeCareServiceManager addOrderComplaintWithId:@"131" shopsId:@"1" comments:@"123" block:^(BOOL success, NSError * _Nullable error) {
                
            }];
            break;
        case 13:
            [HomeCareServiceManager addOrderScoreWithId:@"131" score:5 block:^(BOOL success, NSError * _Nullable error) {
                
            }];
            break;
        default:
            break;
    }
}

- (UIButton *)actionButtonWithTitle:(NSString *)title
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    button.layer.cornerRadius = 20.;
    button.layer.masksToBounds = YES;
    button.layer.borderColor = [UIColor blackColor].CGColor;
    button.layer.borderWidth = 1.;
    return button;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
