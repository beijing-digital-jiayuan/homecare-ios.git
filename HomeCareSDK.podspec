#
# Be sure to run `pod lib lint HomeCareSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'HomeCareSDK'
  s.version          = '1.0.0'
  s.summary          = '家圆养老 HomeCareSDK.'

  s.description      = <<-DESC
  家圆养老，一站式居家养老服务，SDK提供家庭医生和家庭服务功能
  DESC

  s.homepage         = 'https://kinstalk.com/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'cyssan1991' => 'liguibin@shuzijiayuan.com' }
  s.source           = { :git => 'https://gitee.com/beijing-digital-jiayuan/homecare-ios.git', :tag => s.version }

  s.ios.deployment_target = '9.0'

  s.dependency 'TXIMSDK_iOS'
  s.dependency 'TXLiteAVSDK_TRTC',"10.9.13161"

  s.source_files = 'HomeCareSDK/HomeCareSDK.framework/Headers/*.h'
  s.vendored_frameworks = 'HomeCareSDK/*.framework','HomeCareSDK/*.xcframework'
  s.resources = 'HomeCareSDK/resources/*.bundle'
  
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = {
      'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64',
      'ENABLE_BITCODE' => 'NO'
  }
end
