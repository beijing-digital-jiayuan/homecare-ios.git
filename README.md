# HomeCareSDK

[![CI Status](https://img.shields.io/travis/cyssan1991/HomeCareSDK.svg?style=flat)](https://travis-ci.org/cyssan1991/HomeCareSDK)
[![Version](https://img.shields.io/cocoapods/v/HomeCareSDK.svg?style=flat)](https://cocoapods.org/pods/HomeCareSDK)
[![License](https://img.shields.io/cocoapods/l/HomeCareSDK.svg?style=flat)](https://cocoapods.org/pods/HomeCareSDK)
[![Platform](https://img.shields.io/cocoapods/p/HomeCareSDK.svg?style=flat)](https://cocoapods.org/pods/HomeCareSDK)

---
## 介绍

### [家圆养老](https://kinstalk.com/)


* **SDK网站: https://gitee.com/beijing-digital-jiayuan/homecare-ios.git**


|SDK|下载地址|集成指引|
|-|-------:|:------:|
|iOS|https://gitee.com/beijing-digital-jiayuan/homecare-ios|[pod集成]|
|Adnroid|https://gitee.com/beijing-digital-jiayuan/homecare-android|集成|



---
## 安装


### SDK集成

#### [cocoaPods](https://cocoapods.org) 集成

1.本地项目文件夹下，修改`Podfile`文件

```
platform :ios, '9.0'
source 'https://github.com/CocoaPods/Specs.git'

target 'App' do
pod 'HomeCareSDK' :git => 'https://gitee.com/beijing-digital-jiayuan/homecare-ios.git'
end
```

2.终端执行命令，加载AvatarCloudSDK
```
pod install
```

3.如果安装失败，请更新cocoapods的资源配置信息
```
pod repo update
```



---
## 使用


#### 配置


* 在 App 的 Info.plist 文件中添加一个子项目 App Transport Security Settings，然后在其中添加一个 key：Allow Arbitrary Loads，其值为YES。

* 在 App 的 Info.plist 添加授权提示信息。

```
<key>NSPhotoLibraryUsageDescription</key>
<string>应用需要使用相册权限，以便您向医生发送健康资料。</string>
<key>NSCameraUsageDescription</key>
<string>应用需使用相机权限，以便您向医生进行视频咨询。</string>
<key>NSMicrophoneUsageDescription</key>
<string>应用需使用麦克风权限，以便您向医生进行视频咨询。</string>
```

* 如果需要在后台时保持音频通话状态，在 Capabilities -> Background Modes 里勾选 audio, airplay, and Picture in Picture

* 需勾选 Always embed swift standard libraries 为 YES

* iOS 导入资源运行后报错：“Xcode 12.X 版本编译提示 Building for iOS Simulator, but the linked and embedded framework '.framework'...”？
 **将 Build Settings > Build Options > Validate Workspace 改为 Yes，再单击运行。

* 如果报错 ‘ld: warning: Could not find or use auto-linked library 'swiftXXX’, 请在项目内创建并添加一个.swift文件


#### 初始化
* 配置**AppId** 和 **mobile**

```
#import <HomeCareSDK/HomeCareSDK.h>
    
[HomeCareSDKManager initWithAppId:@"" mobile:@"" isDevelop:YES];
```

#### 家庭医生

```
#import <HomeCareSDK/HomeCareSDK.h>

//使用 HomeCareDoctorManager

//呼叫医生
[HomeCareDoctorManager callDoctorWithBlock:^(HCCallingState state) {
    NSLog(@"call state : %lu", (unsigned long)state);
    /*
    HCCallingStart = 0,     //主动拨打
    HCCallingSuccess,       //视频接通
    HCCallingFinish,        //通话结束
    HCCallingCancel,        //拨打取消
    HCCallingFail,          //呼叫失败
    */
}];

//获取病例列表
[HomeCareDoctorManager getRecordListWithSuccess:^(NSArray<HomeCareRecordInfo *> * _Nonnull list) {

} error:^(NSError * _Nonnull error) {
    
}];

```

#### 家圆服务

```
#import <HomeCareSDK/HomeCareSDK.h>

//使用 HomeCareServiceManager

/**
 *  获取服务项目
 *  返回 HomeCareServiceInfo 数组
 */
[HomeCareServiceManager getCategoryListWithSuccess:^(NSArray<HomeCareServiceInfo *> * _Nonnull list) {

} error:^(NSError * _Nonnull error) {

}];

/**
 *  获取服务项目列表
 *  @param categoryId 服务类型Id
 *  返回  bannerList :  HomeCareServiceListInfo 数组
 *       serviceList : HomeCareServiceListInfo 数组
 */
[HomeCareServiceManager getServiceListWithId:@"" success:^(NSArray<HomeCareServiceListInfo *> * _Nonnull bannerList, NSArray<HomeCareServiceListInfo *> * _Nonnull serviceList) {
    
} error:^(NSError * _Nonnull error) {
    
}];

/**
 *  获取地址列表
 *  返回 HomeCareAddressInfo 数组
 */
[HomeCareServiceManager getAddressListWithSuccess:^(NSArray<HomeCareAddressInfo *> * _Nonnull list) {
    
} error:^(NSError * _Nonnull error) {
    
}];

/**
 *  添加地址
 *  @param name      名字
 *  @param mobile    手机号
 *  @param address   地址
 */
[HomeCareServiceManager addAddressWithName:@"名字" mobile:@"手机号" address:@"地址" addressLab:@"备注" block:^(BOOL success, NSError * _Nullable error) {
    
}];

/**
 *  删除地址
 *  @param addressId 地址ID
 */
[HomeCareServiceManager deleteAddressWithId:@"" block:^(BOOL success, NSError * _Nullable error) {
    
}];

/**
 *  编辑更新地址
 *  @param addressId   服务项目ID
 *  @param name        名字
 *  @param mobile      手机号
 *  @param address     地址
 */
[HomeCareServiceManager editAddressWithId:@"" name:@"名字" mobile:@"手机号" address:@"地址" addressLab:@"备注" block:^(BOOL success, NSError * _Nullable error) {
    
}];

/**
 *  获取订单列表
 *  返回 HomeCareOrderInfo 数组
 */
[HomeCareServiceManager getOrderListWithSuccess:^(NSArray<HomeCareOrderInfo *> * _Nonnull list) {
    
} error:^(NSError * _Nonnull error) {
    
}];

/**
 *  新增订单
 *  @param serviceId       服务项目ID
 *  @param receiveName     收件人
 *  @param receiveMobile   收件人手机号
 *  @param receiveAddress  收件人地址
 *  @param orderDesc       备注
 */
[HomeCareServiceManager submitOrderWithId:@"" receiveName:@"名字" receiveMobile:@"手机号" receiveAddress:@"地址" orderDesc:@"备注" block:^(BOOL success, NSError * _Nullable error) {
    
}];

/**
 *  取消订单
 *  @param orderId    订单ID
 */
[HomeCareServiceManager cancelOrderWithId:@"" block:^(BOOL success, NSError * _Nullable error) {
    
}];

/**
 *  订单详情
 *  @param orderId 订单ID
 *  返回 HomeCareOrderInfo 订单信息
 */
[HomeCareServiceManager getOrderDetailWithId:@"" success:^(HomeCareOrderInfo * _Nonnull info) {
    
} error:^(NSError * _Nonnull error) {
    
}];

/**
 *  订单投诉 / 建议
 *  @param orderId  订单ID
 *  @param shopsId  商品ID
 *  @param comments 内容
 */
[HomeCareServiceManager addOrderComplaintWithId:@"" shopsId:@"" comments:@"" block:^(BOOL success, NSError * _Nullable error) {
    
}];

/**
 *  订单评分
 *  @param orderId  订单ID
 *  @param score    评分 整数 0 ~ 5
 */
[HomeCareServiceManager addOrderScoreWithId:@"" score:5 block:^(BOOL success, NSError * _Nullable error) {
    
}];

```

* 更多高级功能配置请参考demo工程相关文档

