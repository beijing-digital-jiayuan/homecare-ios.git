//
//  HomeCareServiceManager.h
//  HomeCareSDK
//
//  Created by King on 2023/6/3.
//

#import <Foundation/Foundation.h>

@class HomeCareServiceInfo;
@class HomeCareServiceListInfo;
@class HomeCareAddressInfo;
@class HomeCareOrderInfo;

/// 回调成功
typedef void (^HCRequestBlock)(BOOL success, NSError * _Nullable error);

NS_ASSUME_NONNULL_BEGIN

@interface HomeCareServiceManager : NSObject

/**
 *  获取服务类别
 */
+ (void)getCategoryListWithSuccess:(void(^)(NSArray<HomeCareServiceInfo *> *list))successBlock error:(void(^)(NSError *error))errorBlock;

/**
 *  获取服务项目列表
 *  @param categoryId 服务类型Id
 */
+ (void)getServiceListWithId:(NSString *)categoryId success:(void(^)(NSArray<HomeCareServiceListInfo *> *bannerList, NSArray<HomeCareServiceListInfo *> *serviceList))successBlock error:(void(^)(NSError *error))errorBlock;

/**
 *  获取地址列表
 */
+ (void)getAddressListWithSuccess:(void(^)(NSArray<HomeCareAddressInfo *> *list))successBlock error:(void(^)(NSError *error))errorBlock;

/**
 *  添加地址
 *  @param name         名字
 *  @param mobile     手机号
 *  @param address   地址
 *  @param addressLab   地址标签
 */
+ (void)addAddressWithName:(NSString *)name mobile:(NSString *)mobile address:(NSString *)address addressLab:(NSString *)addressLab block:(HCRequestBlock)block;

/**
 *  删除地址
 *  @param addressId 地址ID
 */
+ (void)deleteAddressWithId:(NSString *)addressId block:(HCRequestBlock)block;

/**
 *  编辑更新地址
 *  @param addressId   服务项目ID
 *  @param name              名字
 *  @param mobile          手机号
 *  @param address        地址
 *  @param addressLab   地址标签
 */
+ (void)editAddressWithId:(NSString *)addressId name:(NSString *)name mobile:(NSString *)mobile address:(NSString *)address addressLab:(NSString *)addressLab block:(HCRequestBlock)block;


/**
 *  获取订单列表
 */
+ (void)getOrderListWithSuccess:(void(^)(NSArray<HomeCareOrderInfo *> *list))successBlock error:(void(^)(NSError *error))errorBlock;

/**
 *  新增订单
 *  @param serviceId             服务项目ID
 *  @param receiveName        收件人
 *  @param receiveMobile    收件人手机号
 *  @param receiveAddress  收件人地址
 *  @param orderDesc             备注
 */
+ (void)submitOrderWithId:(NSString *)serviceId receiveName:(NSString *)receiveName receiveMobile:(NSString *)receiveMobile receiveAddress:(NSString *)receiveAddress orderDesc:(NSString *)orderDesc block:(HCRequestBlock)block;

/**
 *  取消订单
 *  @param orderId    订单ID
 */
+ (void)cancelOrderWithId:(NSString *)orderId block:(HCRequestBlock)block;

/**
 *  订单详情
 *  @param orderId 订单ID
 */
+ (void)getOrderDetailWithId:(NSString *)orderId success:(void(^)(HomeCareOrderInfo *info))successBlock error:(void(^)(NSError *error))errorBlock;

/**
 *  订单投诉 / 建议
 *  @param orderId   订单ID
 *  @param shopsId   商品ID
 *  @param comments 内容
 */
+ (void)addOrderComplaintWithId:(NSString *)orderId shopsId:(NSString *)shopsId comments:(NSString *)comments block:(HCRequestBlock)block;


/**
 *  订单评分
 *  @param orderId  订单ID
 *  @param score      评分 整数 0 ~ 5
 */
+ (void)addOrderScoreWithId:(NSString *)orderId score:(NSInteger)score block:(HCRequestBlock)block;


@end

//----------返回数据对象----------

@interface HomeCareServiceInfo : NSObject

// 类型Id
@property(nonatomic, strong) NSString *categoryId;
// 类型名称
@property(nonatomic, strong) NSString *name;
// 类型描述
@property(nonatomic, strong) NSString *categoryDesc;

@end



@interface HomeCareServiceListInfo : NSObject

// 服务id
@property(nonatomic, strong) NSString *serviceId;
// 类型id
@property(nonatomic, strong) NSString *categoryId;
// 商户id
@property(nonatomic, strong) NSString *shopsId;
// 服务名称
@property(nonatomic, strong) NSString *name;
// 服务描述
@property(nonatomic, strong) NSString *serviceDesc;
// 服务图片地址
@property(nonatomic, strong) NSString *imgUrl;
// 服务价格(单位：分)
@property(nonatomic, strong) NSString *price;
// 支付类型 0线下 1线上
@property(nonatomic, strong) NSString *payType;
// 服务状态 0未上架，1已上架
@property(nonatomic, strong) NSString *serviceStatus;
// 创建时间
@property(nonatomic,assign) long long createTime;
// 类型 1:正常服务 2:活动付费服务 3:活动免费服务
@property(nonatomic, strong) NSString *serviceType;
// 服务电话
@property(nonatomic, strong) NSString *shopMobile;
// 服务价格(单位：元)
@property(nonatomic, strong) NSString *priceYuan;

@end



@interface HomeCareAddressInfo : NSObject

// 地址id
@property(nonatomic, strong) NSString *addressId;
// 名字
@property(nonatomic, strong) NSString *name;
// 手机号
@property(nonatomic, strong) NSString *mobile;
// 地址
@property(nonatomic, strong) NSString *address;

@end



@interface HomeCareOrderInfo : NSObject

// 订单id
@property(nonatomic, strong) NSString *orderId;
// 订单编号
@property(nonatomic, strong) NSString *orderNo;
// 服务id
@property(nonatomic, strong) NSString *serviceId;
// 商户id
@property(nonatomic, strong) NSString *shopsId;
// 服务名称
@property(nonatomic, strong) NSString *serviceName;
// 用户id
@property(nonatomic, strong) NSString *userId;
// 订单状态 -1:已取消 0:未付款 1:待接单 3:待上门 4:已完成 5:关闭订单
@property(nonatomic, strong) NSString *orderStatus;
// 支付类型 0线下 1线上
@property(nonatomic, strong) NSString *payType;
// 订单备注
@property(nonatomic, strong) NSString *orderDesc;
// 创建时间
@property(nonatomic,assign) long long createTime;
// 服务价格(单位：元)
@property(nonatomic, strong) NSString *totalMoneyYuan;
// 收货人名称
@property(nonatomic, strong) NSString *receiveName;
// 收货人手机号码
@property(nonatomic, strong) NSString *receiveMobile;
// 收货地址
@property(nonatomic, strong) NSString *receiveAddress;
// 上门时间
@property(nonatomic,assign) long long visitTime;
// 上门师傅名字
@property(nonatomic, strong) NSString *visitName;
// 上门师傅手机号码
@property(nonatomic, strong) NSString *visitMobile;
// 上门师傅头像
@property(nonatomic, strong) NSString *visitAvatar;

@end

NS_ASSUME_NONNULL_END
