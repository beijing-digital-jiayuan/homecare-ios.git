//
//  HomeCareDoctorManager.h
//  HomeCareSDK
//
//  Created by King on 2023/6/3.
//

#import <Foundation/Foundation.h>

@class HomeCareRecordInfo;

typedef NS_ENUM (NSUInteger, HCCallingState) {
    HCCallingStart = 0,     //主动拨打
    HCCallingSuccess,       //视频接通
    HCCallingFinish,        //通话结束
    HCCallingCancel,        //拨打取消
    HCCallingFail,          //呼叫失败
};

NS_ASSUME_NONNULL_BEGIN

@interface HomeCareDoctorManager : NSObject

/**
 呼叫医生
 */
+ (void)callDoctorWithBlock:(void(^)(HCCallingState state))block;

/**
 获取病例列表
 */
+ (void)getRecordListWithSuccess:(void(^)(NSArray<HomeCareRecordInfo *> *list))successBlock error:(void(^)(NSError *error))errorBlock;;

@end

@interface HomeCareRecordInfo : NSObject

//使用者
@property (nonatomic, strong) NSString *patientName;
//诊断时间
@property (nonatomic, assign) long long createTime;
//诊断
@property (nonatomic, strong) NSString *diagnose;
//疾病描述
@property (nonatomic, strong) NSString *diseaseDescn;
//医生科室
@property (nonatomic, strong) NSString *doctorDept;
//医生名字
@property (nonatomic, strong) NSString *doctorName;
//详情地址
@property (nonatomic, strong) NSString *detailWebUrl;

@end


NS_ASSUME_NONNULL_END
