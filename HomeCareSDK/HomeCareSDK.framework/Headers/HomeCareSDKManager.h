//
//  HomeCareSDKManager.h
//  HomeCareSDK
//
//  Created by King on 2023/5/30.
//

#import <Foundation/Foundation.h>

@interface HomeCareSDKManager : NSObject

//default 60
@property (nonatomic, assign) NSTimeInterval timeoutInterval;

+ (void)initWithAppId:(NSString *)appID mobile:(NSString *)mobile isDevelop:(BOOL)isDevelop;
+ (NSString *)getVersion;

@end

